const Sequelize =   require('sequelize');
const sequelize =  require('../config/index');

const User  = sequelize.define('user',{

     id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            allowNull:false,
            primaryKey:true
     },
     name:{
            type:Sequelize.STRING,
            allowNull:false
     },
     email:{

            type:Sequelize.STRING,
            allowNull:false
     },
     role:{

            type:Sequelize.STRING,
            defaultValue:"user",
            value:["user","admin","superadmin"],
            allowNull:false
            
     },
     username:{

            type:Sequelize.STRING,
            allowNull:false
     },
     password:{
            type:Sequelize.STRING,
            allowNull:false        

     }

});

module.exports  = User;