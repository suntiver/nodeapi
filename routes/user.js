const route = require('express').Router();
const feedController = require('../controllers/feedataController');


//Bring in the User Registration function
const  {userRegister, userLogin,userAuth, serializeUser,checkRole}  = require("../utils/Auth");


//User Registeration Route 
route.post("/register-user",async(req,res) => {

        await userRegister(req.body,"user",res);

});
//Admin Registration Route
route.post("/register-admin",async(req,res) => {

        await userRegister(req.body,"admin",res);

});
//Super Admin Registration Route
route.post("/register-super-admin",async(req,res) => {

        await userRegister(req.body,"superadmin",res);

});

//Users Login Route
route.post("/login-user",async(req,res) => {

        await userLogin(req.body,"user",res);

});
//Admin Login Route
route.post("/login-admin",async(req,res) => {

        await userLogin(req.body,"admin",res);

});
//Super Admin Route
route.post("/login-super-admin",async(req,res) => {

        await userLogin(req.body,"superadmin",res);
});


//Profile Route
route.get("/profile",userAuth,async(req,res) => {

        return res.json(serializeUser(req.user));

});

//Users Protected Route
route.get("/user-protectd",userAuth,checkRole(['user']),feedController.getPosts);


// route.get("/user-protectd/:id",userAuth,checkRole(['user']),async(req,res) => {

//         return res.json(req.params.id);

// });

//Admin Protected Route
route.get("/admin-protectd",userAuth,checkRole(['admin']),async(req,res) => {

        return res.json("admin-protectd");
});
//Super Admin  Protected Route
route.get("/super-admin-protectd",userAuth,checkRole(['superadmin']),async(req,res) => {

        return res.json("super-admin-protectd");

});


//Super Admin  and  admin  Protected Route
route.get("/super-admin-and-admin-protectd",userAuth,checkRole(['superadmin','admin']),async(req,res) => {

        return res.json("super-admin-and-admin-protectd");

});


module.exports = route;