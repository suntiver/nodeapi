require("dotenv").config();

module.exports = { 
    
    SECRET:process.env.APP_SECRET,
    DB    :process.env.APP_DB,
    USER  :process.env.USER,
    PASS :process.env.PASS,
    DIALECT :process.env.DIALECT,
    HOST  :process.env.HOST

}