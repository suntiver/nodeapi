

const  cors = require("cors");
const express =  require("express");
const bodyParser  = require("body-parser");
const  {success,error}  = require("consola");
const passport = require('passport');




const connect_sequelize =   require('./config/index');
const  modelUser  =  require('./models/User');


const app = express();


app.use(cors());
app.use(bodyParser.json());
app.use(passport.initialize());


require('./middlewares/passport')(passport);


// modelUser.sequelize.sync();



//User Router Middleware
app.use('/api/users',require('./routes/user'));



const  PORT  =  4000;
app.listen(PORT,() =>

            success({message:`Server  started on PORT  ${PORT}`,badge:true})

);