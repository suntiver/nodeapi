const  bcrypt =  require('bcryptjs');
const  User  = require("../models/User");
const  jwt = require('jsonwebtoken');
const passport = require('passport');

const {SECRET} = require('../config/secret')

 /**
 *@DESC To register the user (ADMIN,SUPER_ADMIN,USER)
 */
 const userRegister = async(userDets,role,res) =>{

    try{
            //validate the username
           
            let  usernameTaken  =  await validateUsername(userDets.username);
            console.log(usernameTaken);
            if(!usernameTaken){
                return res.status(400).json({

                    message:`Username is already taken.`,
                    success:false
                });
            }


            //validate the email
            let emailNotRegistered = await validateEmail(userDets.email);
      
            if(!emailNotRegistered){
                return res.status(400).json({

                    message:`Email  is already  registered.`,
                    success:false
                });
            }


            //Get the  hashed password
            const password = await bcrypt.hash(userDets.password,12);

            //create a new user
            const newUser =   await User.create({
                ...userDets,
                password,
                role
            });
            

            return  res.status(201).json({

                message:"Now you are successfully registred.Please login",
                success:true


            });



    }catch(err){

            return  res.status(500).json({

                message:"Unable to create your account.",
                success:false


            });


    }
   
     


 };


 /**
 *@DESC To Login the user (ADMIN,SUPER_ADMIN,USER)
 */
const  userLogin = async (userCreds,role,res) =>{
    
   
    let {username,password} =  userCreds;
    
    //First Check if the username is in the database
    const user = await User.findOne({
            where: { username: username }
    });

    if(!user){

        return res.status(404).json({
                
                message:"Username is not found.Invalid login creadentials.",
                success:false

        });
    }

    if(user.role != role){

        return res.status(403).json({
                
            message:"Please make sure you are logging in from the right portal.",
            success:false

        });

    }

    //That means user is existing and trying to signin for the right portal
    //Now check for the password
    let isMatch  = await  bcrypt.compare(password,user.password);

    if(isMatch){
            // Sign in the token and issue it to the user

            let token   = jwt.sign({
                user_id:user.id,
                role:user.role,
                username:user.username,
                email:user.email
            },SECRET,{ expiresIn:"7 days"});

            let result = {
                user_id:user.id,
                username:user.username,
                role:user.role,
                email:user.email,
                token:`Bearer ${token}`,
                expiresIn:168
            };

            return res.status(200).json({
                
                ...result,
                message:"You are now logged in.",
                success:true

            });

    }else{

            return res.status(403).json({
                    
                message:"Incorrect  password.",
                success:false

            });

    }

};


 const validateUsername  = async (username) =>{
   
    const user =   await User.findOne({
        where: { username: username }

    });
    
   
    
    return  user ? false:true;

 };


 /**
 *@DESC Passport middleware
 */
const userAuth  = passport.authenticate("jwt",{session:false});


 /**
 *@DESC Passport middleware
 */
const checkRole = role => (req,res,next) =>{

    if(role.includes(req.user.role)){

        return next();
    }

    return res.status(401).json({

        message:"Unauthorized",
        success:false
        
    });

};



 const validateEmail = async (emailaddress) =>{

    const email =   await User.findOne({
        where: { email:emailaddress  }

    });
   
    return  email ? false:true;

 };


const  serializeUser = user => {
        return {
            username:user.username,
            email:user.email,
            name:user.name,
            id:user.id,
            updatedAt:user.updatedAt,
            createdAt:user.createdAt
        };

};


 module.exports = { 
     checkRole,
     userAuth,
     userLogin,
     userRegister,
     serializeUser

    };